package com.demo.vowels;

import com.demo.vowels.domain.VowelCombination;
import com.demo.vowels.service.WordService;
import com.demo.vowels.service.impl.WordServiceImpl;
import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
class VowelsApplicationTests {

//    @InjectMocks
    private static WordService wordService;

    static String absolutePath;
    static List<String> words;

    @BeforeAll
    public static void init() {
        wordService = new WordServiceImpl();

        ClassLoader classLoader = VowelsApplicationTests.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource("input_test.txt")).getFile());
        absolutePath = file.getAbsolutePath();
        assertTrue(absolutePath.endsWith("/input_test.txt"));

        words = wordService.readFile(absolutePath);
    }

    @Test
    void contextLoads() {
    }

    @Test
    void testCalculatedCombinationContainsGivenVowels() {

        List<VowelCombination> vowelCombinationList = wordService.calculateAvgVowelPerWord(words);
        VowelCombination vc1 = new VowelCombination(6);
        Set<Character> vowelSet1 = new HashSet<>();
        vowelSet1.add('a');
        vowelSet1.add('o');
        vc1.setVowelSet(vowelSet1);
        vc1.setTotalVowels(2);

        VowelCombination vc2 = new VowelCombination(4);
        Set<Character> vowelSet2 = new HashSet<>();
        vowelSet2.add('a');
        vowelSet2.add('e');
        vc2.setVowelSet(vowelSet2);
        vc2.setTotalVowels(2);

        assertTrue(vowelCombinationList.contains(vc1));
        assertTrue(vowelCombinationList.contains(vc2));
    }

    @Test
    void testVowelCombinationIsAsExpected() {
        List<VowelCombination> vowelCombinationList = wordService.calculateAvgVowelPerWord(words);

        Assertions.assertEquals(vowelCombinationList.get(0).toString(), "({a, o}, 6) -> 2");
        Assertions.assertEquals(vowelCombinationList.get(1).toString(), "({a, e}, 4) -> 2");
        Assertions.assertEquals(vowelCombinationList.get(2).toString(), "({a, o}, 6) -> 3");
    }
}
