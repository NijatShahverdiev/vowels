package com.demo.vowels.util;

import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;

public class CommonConfig {

    public static String filePath(String fileName) throws IOException {
        File file = ResourceUtils.getFile(fileName);
        return file.getAbsolutePath();
    }
}
