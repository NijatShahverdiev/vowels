package com.demo.vowels;

import com.demo.vowels.domain.VowelCombination;
import com.demo.vowels.service.WordService;
import com.demo.vowels.util.CommonConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class VowelsApplication implements CommandLineRunner {

    @Value("classpath:input.txt")
    String fileName;

    private final WordService wordService;

    public static void main(String[] args) {
        SpringApplication.run(VowelsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        String filePath = CommonConfig.filePath(fileName);
        List<String> words = wordService.readFile(filePath);
        List<VowelCombination> avgVowelPerWord = wordService.calculateAvgVowelPerWord(words);
        log.info("{} ", words);
        wordService.writeToFile(avgVowelPerWord);
        log.info("{}", "Average number of vowels per word has written to output.txt");
    }
}
