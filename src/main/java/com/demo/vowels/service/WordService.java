package com.demo.vowels.service;

import com.demo.vowels.domain.VowelCombination;

import java.io.IOException;
import java.util.List;

public interface WordService {

    List<VowelCombination> calculateAvgVowelPerWord(List<String> words);
    
    List<String> readFile(String path);

    void writeToFile(List<VowelCombination> wordModels) throws IOException;
}
