package com.demo.vowels.service.impl;

import com.demo.vowels.domain.VowelCombination;
import com.demo.vowels.service.WordService;
import com.demo.vowels.util.CommonConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;

/**
 * @author Nijat Shahverdiev
 */

@Service
@Slf4j
public class WordServiceImpl implements WordService {

    @Value("classpath:output.txt")
    String fileName;

    @Override
    public List<String> readFile(String path) {
        List<String> words = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String line = reader.readLine();
            while (line != null) {
                String[] wordArr = line.split("[^a-zA-Z\\-']");
                for (String w : wordArr) {
                    if (w.matches("^[a-zA-Z\\-']+$"))
                        words.add(w.toLowerCase());
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return words;
    }

    @Override
    public void writeToFile(List<VowelCombination> wordModels) throws IOException {
        String filePath = CommonConfig.filePath(fileName);
        try (PrintWriter writer = new PrintWriter(filePath, "UTF-8")) {
            for (VowelCombination w : wordModels) {
                writer.println(w);
            }
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            log.trace("Exception happened during writing file {}", e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public List<VowelCombination> calculateAvgVowelPerWord(List<String> wordList) {
        List<String> addedWords = new ArrayList<>();
        List<VowelCombination> vowelCombinationList = new ArrayList<>();
        for (String s : wordList) {
            final VowelCombination vowelCombination = new VowelCombination(s.length());
            int vowelCount = 0;
            checkVowel(s, vowelCombination, vowelCount);
            if (addedWords.contains(s)) {
                UpdateSameCombination(vowelCombinationList, vowelCombination, vowelCount);
            } else {
                addedWords.add(s);
                vowelCombinationList.add(vowelCombination);
            }
        }
        return vowelCombinationList;
    }


    private void checkVowel(String s, VowelCombination vc, int vowelCount) {
        for (char c : s.toCharArray()) {
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u')
                vc.appendVowels(c, ++vowelCount);
        }
    }

    private void UpdateSameCombination(List<VowelCombination> vcList, VowelCombination vc, int vowelCount) {
        for (VowelCombination v : vcList) {
            if (v.equals(vc)) {
                vc.increaseCount();
                vc.setTotalVowels(vowelCount);
                break;
            }
        }
    }
}
