package com.demo.vowels.domain;


import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Nijat Shahverdiyev
 * 2023.02.02
 */
public class VowelCombination {
    private Set<Character> vowelSet = new TreeSet<>();
    private final int wordLength;
    private double totalVowels;
    private int count = 1;

    public VowelCombination(int wordLength) {
        this.wordLength = wordLength;
    }

    public void setVowelSet(Set<Character> vowelSet) {
        this.vowelSet = vowelSet;
    }

    public void increaseCount() {
        this.count++;
    }

    public void setTotalVowels(int vowelsCount) {
        this.totalVowels += vowelsCount;
    }

    public void appendVowels(char c, int totalVowels) {
        vowelSet.add(c);
        this.totalVowels = totalVowels;
    }


    @Override
    public int hashCode() {
        return Objects.hash(vowelSet, wordLength);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VowelCombination)) return false;
        VowelCombination wordModel = (VowelCombination) o;
        return wordLength == wordModel.wordLength && vowelSet.equals(wordModel.vowelSet);
    }


    @Override
    public String toString() {
        DecimalFormat formatter = new DecimalFormat("#.##");
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        Iterator<Character> iterator = vowelSet.iterator();
        if (!iterator.hasNext()) {
            sb.append("{}");
        }
        else {
            sb.append("{");
            while (true) {
                sb.append(iterator.next());
                if (!iterator.hasNext()) {
                    sb.append('}');
                    break;
                }
                sb.append(',').append(' ');
            }
        }
        String avgVowel = formatter.format(totalVowels / count);
        sb.append(", ").append(wordLength).append(") -> ").append(avgVowel);
        return sb.toString();
    }
}
